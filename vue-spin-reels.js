const template = `
	<div class=slot-machine>
		<div class=slot-machine-box>
			<template class=slot-machine-slot v-for='(slot, idx) in slots'>
				<div class=slot-machine-window>

					<div class=slot-machine-glass></div>

					<div class=slot-machine-wrap ref=slots>
						<div class='slot-machine-item slot-machine-item--copy'>
							<img :src='slot[2].image' alt :height=height/2 :width=height/2>
						</div>

						<div class=slot-machine-item v-for='opt in slot'>
							<img :src='opt.image' alt :height=height/2 :width=height/2>
						</div>

						<div class='slot-machine-item slot-machine-item--copy'>
							<img :src='slot[0].image' alt :height=height/2 :width=height/2>
						</div>

						<div class='slot-machine-item slot-machine-item--copy'>
							<img :src='slot[1].image' alt :height=height/2 :width=height/2>
						</div>
					</div>
				</div>

				<div v-if='idx +  1 < slots.length' class=slot-machine-bar></div>
			</template>
		</div>
	</div>
`

export default {
	template,

	data: function() {
		return {
			spinning: false,

			choices: [],

			slots: []
		}
	},

	props: {
		speed: {
			type: Number,
			default: 250
		},
		height: {
			type: Number,
			default: 200
		},
		spins:
		{
			type: Number,
			default: 5
		},
		config: {
			type: [
				String,
				Object
			],
			default: 'vue-spin-reels-data.json'
		}
	},

	computed: {
		win () {
			return this.choices.every(val => val === this.choices[0])
		}
	},

	methods: {
		start (choice) {
			if (this.spinning)
				return
			else
				this.spinning = true

			if (!isNaN(choice))
				this.choices = [...Array(this.slots.length)].fill(choice)
			else {
				this.choices = this.slots
					.map(slot =>
						Math.floor(Math.random() * (slot.length + 1)) )

				// Removes any random wins by incrementing / decrementing the last reel
				if (!!choice && this.choices.every((n, i, a) => n === a[0]))
					this.choices[this.slots.length - 1] = this.choices[this.slots.length - 1]
						===  this.choices[this.slots.length - 1].length
							? this.choices[this.slots.length - 1] - 1
							: this.choices[this.slots.length - 1] + 1
			}

			this.play()
		},

		play () {
			this.$refs.slots.forEach((slot, idx, {length}) => {
				const timingOffset = Math.floor(
					(this.speed * 2)
					- (this.slots[idx].length + 1)
					- this.choices[idx]
					/ this.slots[idx].length
					* (this.speed * 2) )

				Object.assign(slot.style, {
					'transform': 'unset',
					'transition-property': 'unset',
					'transition-duration': 'unset',
					'transform': `translateY(calc(-100% + ${this.height * 2}px))`,
					'animation-duration': `${this.speed}ms`,
					'animation-direction': 'reverse',
					'animation-fill-mode': 'backwards',
					'animation-iteration-count': this.spins + (idx * 2),
					'animation-timing-function': 'ease-out',
					'animation-name': 'spinning'
				})

				const startiter = () => {
					Object.assign(slot.style, {
						'animation-timing-function': 'linear'
					})

					slot.removeEventListener('animationiteration', startiter)
				}

				slot.addEventListener('animationiteration', startiter)

				const animend = () => {
					Object.assign(slot.style, {
						'animation-name': 'unset',
						'transition-property': 'transform',
						'transition-duration': `${ timingOffset }ms`,
						'transform': `translateY(-${this.choices[idx] * (this.height / 2)}px)`
					})

					this.bell.load()

					this.bell.play()

					if (idx + 1 === length)
						this.end()

					slot.removeEventListener('animationend', animend)
				}

				slot.addEventListener('animationend', animend)
			})
		},

		end () {
			// TODO - JN - Rearrange items before spin
			this.slots.forEach(items => {

			})

			if (this.win) {
				this.jackpot.play()

				this.$refs.slots.forEach(slot  => {
					Object.assign(slot.style, {
						'animation-name': 'flashing',
						'animation-delay': '500ms',
						'animation-duration': '150ms',
						'animation-fill-mode': 'forwards',
						'animation-iteration-count': 5,
						'animation-timing-function': 'ease-in-out'
					})

					const endFlashing = () => {
						[
							'animation-name',
							'animation-delay',
							'animation-duration',
							'animation-fill-mode',
							'animation-iteration-count',
							'animation-timing-function'
						].map(prop => {
							slot.style.removeProperty(prop)
						})


						slot.removeEventListener('animationend', endFlashing)
					}

					slot.addEventListener('animationend', endFlashing)
				})

				this.over(this.win)
			} else {
				this.over(this.win)
			}
		},

		over (result) {
			this.spinning = false
			this.$emit('spinEnded', result)
		}

	},

	mounted () {
		this.$root.$on("startSpin", spin => {
			if (!isNaN(spin))
				this.start(spin)
			else
				this.start();
		})

		this.$el.style.setProperty('--slot-height', `${this.height}px`)
	},

	created () {
		if (this.config.constructor === Object) {
			this.bell = config.bell
			this.jackpot = config.jackpot
			this.slots = config.slots
		} else if (this.config.constructor === String) {
			fetch(this.config)
				.then(resp => resp.json())
				.then(({ sounds, slots }) => {
					this.bell = new Audio(sounds.bell)

					this.jackpot = new Audio(sounds.jackpot)

					this.slots = slots
				}).catch(console.log)
		}
	}
}

