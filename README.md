# Vue Spin Reels

## Introduction

Vue Spin Reels is a Vue component the recreats the spinner element of a casino slot machine.

## Installation


## Usage

Uses a Vue `$root` emit event 'startSpin' to begin the spinner and emits an event 'spinEnded' with a single boolean value.

### example

```javascript
	<template>
		<spin-reels @spinEnded=complete></spin-reels>

		<button @click=$root.$emit('startSpin')></button>
	</template>

	<script>
		import SpinReels from 'vue-spin-reels'

		export default {
			components: {
				'spin-reels': SpinReels
			},
			methods: {
				complete (status) {
					console.log('Win? ' + status) // true || false
				}
			}
		}
	</script>
```
